<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCakeIngredientRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cake_ingredient_relations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cake_id')->unsigned()->index();
            $table->integer('ingredient_id')->unsigned()->index();
            $table->double('quantity');
            $table->double('cost');
            $table->timestamps();

            $table->foreign('ingredient_id')
                  ->references('id')
                  ->on('ingredients')
                  ->onDelete('cascade');

            $table->foreign('cake_id')
                  ->references('id')
                  ->on('cakes')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cake_ingredient_relations');
    }
}
