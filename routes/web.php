<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('users/data', 'UserController@loadData');
Route::resource('users', 'UserController');

Route::get('ingredient_categories/data', 'IngredientCategoryController@loadData');
Route::resource('ingredient_categories', 'IngredientCategoryController');

Route::get('ingredients/data', 'IngredientController@loadData');
Route::resource('ingredients', 'IngredientController');

Route::get('cakes/getIngredientsData', 'CakeController@getIngredientsData');
Route::get('cakes/data', 'CakeController@loadData');
Route::resource('cakes', 'CakeController');

Route::get('orders/getCakesData', 'OrderController@getCakesData');
Route::get('orders/data', 'OrderController@loadData');
Route::resource('orders', 'OrderController');

Route::get('billings/getOrderData', 'BillingController@getOrderData');
Route::get('billings/data', 'BillingController@loadData');
Route::get('billings/paid/{billing}', 'BillingController@paid');
Route::resource('billings', 'BillingController');
