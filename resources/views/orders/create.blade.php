@extends('layouts.backoffice_layout')

@section('css')
  <link rel="stylesheet" href="{!! asset('admin_layouts/plugins/select2/select2.min.css') !!}">
@endsection

@section('title')
  Order
@endsection

@section('sub_title')
  Tambah Order
@endsection

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Form Order</h4>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>
              <div class="box-body">
              <form action="{{ url('orders') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                  <div class="col-md-2">
                    <label>Mitra: </label>
                  </div>
                  <div class="col-md-7">
                      <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
                          <select class="form-control" name="user_id">
                            @foreach ($users as $user)
                              <option value="{{ $user->id }}">{{ $user->name }}</option>
                            @endforeach
                          </select>
                          <small class="text-danger">{{ $errors->first('user_id') }}</small>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Kue: </label>
                  </div>
                  <div class="col-md-7">
                    <div class="form-group{{ $errors->has('cakes_id') ? ' has-error' : '' }}">
                        <select class="form-control select2" id="cakes" multiple name="cakes_id">
                          @foreach ($cakes as $cake)
                            <option value="{{ $cake->id }}">{{ $cake->name }}</option>
                          @endforeach
                        </select>
                        <small class="text-danger">{{ $errors->first('cakes_id') }}</small>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <button type="button" class="btn btn-primary" id="submitCake">
                      Submit
                    </button>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Tipe Order:</label>
                  </div>
                  <div class="col-md-4">
                    <select class="form-control" id="category" name="category">
                      <option value="direct-order">Direct-Order</option>
                      <option value="pre-order">Pre-Order</option>
                    </select>
                  </div>
                </div>
                <br>
                <div class="row">
                  <div class="col-md-2">
                    <label>Tanggal Pengambilan:</label>
                  </div>
                  <div class="col-md-4">
                    <input type="date" class="form-control" id="pick_date" name="pick_date" value="{{ date('Y-m-d') }}" readonly>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Kue</h4>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>
              <div class="box-body" id="cakes_data">

              </div>
            </div>
          </div>
        </div>

      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Total Harga</h4>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4">
                      <div class="form-group{{ $errors->has('total_price') ? ' has-error' : '' }}">
                          <input type="text" class="form-control" id="total_price" name="total_price" value="{{ old('total_price') }}" placeholder="Total Harga" readonly>
                          <small class="text-danger">{{ $errors->first('total_price') }}</small>
                      </div>
                  </div>
                  <div class="col-md-3">
                    <button type="button" class="btn btn-success" id="calculatePrice">
                      Hitung Total Harga
                    </button>
                  </div>
                </div>
              </div>
              <div class="box-footer">
                <input type="hidden" name="cakes_summary" value="">
                <button type="submit" class="btn btn-primary pull-right">
                  Simpan
                </button>
                </form>
              </div>
            </div>
          </div>
        </div>
@endsection


@section('js')
  <script src="{!! asset('admin_layouts/plugins/select2/select2.full.min.js') !!}"></script>
  <script type="text/javascript">
    $('.select2').select2()

    $("#cakes").select2({
      placeholder : 'Kue'
    })

    $('#submitCake').on('click', () => {
      console.log($('#cakes').val());
      $.ajax({
        type: 'GET',
        url: '{{ url("orders/getCakesData") }}',
        data: {cakes: $('#cakes').val()},
        success: (data) => {
          $('#cakes_data').html(data)
        },
        error: (error) => {
          console.log(error);
        }
      })
    })

    $('#calculatePrice').on('click', () => {
      total_price = 0
      cakes = []
      quantity = []
      price = []

      $('.cakes_quantity').each(function(index) {
          cakes[index] = $('.cakes_id:eq('+index+')').val()
          quantity[index] = $(this).val()
          price[index] = $('.cakes_price:eq('+index+')').val()

          total_price += ($(this).val() * parseInt($('.cakes_price:eq('+index+')').val()))
      })

      $('input[name=cakes_summary]').val(cakes +';'+ quantity +';'+ price)

      $('#total_price').val(total_price)
    })

    $("#category").on('change', () => {
      category = $("#category").val()
      if (category) {
        $('#pick_date').attr('readonly', false)
      }else {
        $('#pick_date').attr('readonly', true)
      }
    })
  </script>
@endsection
