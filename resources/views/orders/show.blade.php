@extends('layouts.backoffice_layout')

@section('title')
  Home
@endsection

@section('sub_title')
  Detail Order
@endsection

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-9">
                    <h4>Detail Order</h4>
                  </div>
                  <div class="col-md-2">
                    {{-- <button type="button" class="btn btn-success">Paid</button>
                    <button type="button" class="btn btn-danger">Cancel</button> --}}
                  </div>
                </div>
              </div>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-3">
                    <b>Nomor Order: </b>
                  </div>
                  <div class="col-md-8">
                    <b>{{ $order->id }}</b>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    Nama Pemesan:
                  </div>
                  <div class="col-md-8">
                    {{ $order->buyer->name }}
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    Tipe Order:
                  </div>
                  <div class="col-md-8">
                    {{ $order->category }}
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    Tanggal Order:
                  </div>
                  <div class="col-md-8">
                    {{ date('d M Y', strtotime($order->order_date)) }}
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    Tanggal Pengambilan:
                  </div>
                  <div class="col-md-8">
                    {{ date('d M Y', strtotime($order->pick_date)) }}
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <h4 class="box-title">Detail Pesanan</h4>
              </div>
              <div class="box-body">
                <table class="table table-responsive table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>No.</th>
                      <th>Nama</th>
                      <th>Jumlah</th>
                      <th>Harga Modal</th>
                      <th>Harga Jual</th>
                      <th>Total</th>
                    </tr>
                  </thead>
                  <tbody>
                    @php($salim = 1)
                    @php($total_price = 0)
                    @foreach ($order->details as $detail)
                      <tr>
                        <td>{{ $salim++ }}</td>
                        <td>{{ $detail->cake->name }}</td>
                        <td>{{ $detail->quantity }}</td>
                        <td>{{ number_format($detail->cost) }}</td>
                        <td>{{ number_format($detail->price) }}</td>
                        <td>{{ number_format($detail->quantity * $detail->price) }}</td>
                      </tr>
                      @php($total_price += $detail->quantity * $detail->price)
                    @endforeach
                    <tr>
                      <th colspan="5" class="box box-success" style="text-align: right">Total Harga</th>
                      <th class="box box-success">{{ number_format($total_price) }}</th>
                    </tr>
                  </tbody>
                </table>
              </div>
          </div>
        </div>
      </div>
@endsection
