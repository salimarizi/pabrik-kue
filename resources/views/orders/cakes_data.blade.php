@php($salim = 0)
@foreach ($cakes as $cake)
  <div class="row">
    <div class="col-md-3">
      <input type="hidden" class="cakes_id" name="" value="{{ $cake->id }}">
      <label>Nama Kue: {{ $cake->name }} C</label>
    </div>
    <div class="col-md-2">
      <div class="row">
        <div class="col-md-4">
          <label>Jumlah: </label>
        </div>
        <div class="col-md-8">
            <div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
                <input type="number" class="form-control cakes_quantity" min="1" name="quantity" value="{{ old('quantity') }}" placeholder="Jumlah" required>
                <small class="text-danger">{{ $errors->first('quantity') }}</small>
            </div>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="row">
        <div class="col-md-4">
          <label>Harga Modal: </label>
        </div>
        <div class="col-md-8">
            <div class="form-group{{ $errors->has('cost') ? ' has-error' : '' }}">
                <input type="text" class="form-control cakes_cost" name="cost" value="{{ $cake->cost }}" placeholder="Harga Modal" readonly required>
                <small class="text-danger">{{ $errors->first('cost') }}</small>
            </div>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="row">
        <div class="col-md-4">
          <label>Harga Jual: </label>
        </div>
        <div class="col-md-8">
            <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                <input type="number" class="form-control cakes_price" min="{{ $cake->cost }}" name="price" value="{{ $cake->cost }}" placeholder="Harga Jual" required>
                <small class="text-danger">{{ $errors->first('price') }}</small>
            </div>
        </div>
      </div>
    </div>
  </div>
  @php($salim++)
@endforeach
