@extends('layouts.backoffice_layout')

@section('css')
  <link rel="stylesheet" href="{!! asset('admin_layouts/plugins/select2/select2.min.css') !!}">
@endsection

@section('title')
  Tagihan
@endsection

@section('sub_title')
  Tambah Tagihan
@endsection

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Form Tagihan</h4>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>
              <div class="box-body">
              <form action="{{ url('billings') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                  <div class="col-md-2">
                    <label>Mitra: </label>
                  </div>
                  <div class="col-md-5">
                      <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
                          <select class="form-control" id="user_id" name="user_id">
                            @foreach ($users as $user)
                              <option value="{{ $user->id }}">{{ $user->name }}</option>
                            @endforeach
                          </select>
                          <small class="text-danger">{{ $errors->first('user_id') }}</small>
                      </div>
                  </div>
                  <div class="col-md-2">
                    <button type="button" class="btn btn-primary" id="submitMember">
                      Get Billing
                    </button>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Detail Tagihan</h4>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>
              <div class="box-body" id="billing_data">

              </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-success">Simpan Tagihan</button>
              </form>
              </div>
            </div>
          </div>
        </div>
@endsection


@section('js')
  <script src="{!! asset('admin_layouts/plugins/select2/select2.full.min.js') !!}"></script>
  <script type="text/javascript">
    $('.select2').select2()

    $('#submitMember').on('click', () => {
      $.ajax({
        type: 'GET',
        url: '{{ url("billings/getOrderData") }}',
        data: {user_id: $('#user_id').val()},
        success: (data) => {
          $('#billing_data').html(data)
        },
        error: (error) => {
          console.log(error);
        }
      })
    })
  </script>
@endsection
