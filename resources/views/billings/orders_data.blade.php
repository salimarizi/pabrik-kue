@php($salim = 1)
<table class="table table-bordered table-striped">
  <thead>
    <tr>
      <th>No.</th>
      <th>Nomor Order</th>
      <th>Total Tagihan</th>
      <th>Deskripsi Pesanan</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($orders as $order)
      <tr>
        <td>{{ $salim++ }}</td>
        <td>{{ $order->id }}</td>
        <td>{{ number_format($order->details->sum('price')) }}</td>
        <td>
          <ul>
            @foreach ($order->details as $detail)
              <li>
                {{ $detail->cake->name }} <br>
                Rp. {{ number_format($detail->price) }} x {{ $detail->quantity }}pc(s) <br>
                Total = {{ number_format($detail->price * $detail->quantity) }}
              </li>
            @endforeach
          </ul>
        </td>
      </tr>
    @endforeach
  </tbody>
</table>
