@extends('layouts.backoffice_layout')

@section('title')
  Tagihan
@endsection

@section('sub_title')
  List Tagihan
@endsection

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Daftar Tagihan</h4>
                  </div>
                  <div class="col-md-2">
                    @if (Auth::user()->role == 'staff')
                      <a href="{{ url('billings/create') }}" class="btn btn-primary">
                        <i class="fa fa-plus"></i> Buat Tagihan
                      </a>
                    @endif
                  </div>
                </div>
              </div>
              <div class="box-body">
                <table id="table" class="table table-bordered table-striped table-hover">
                  <thead>
                    <tr>
                      <th>No.</th>
                      <th>Nama debitur</th>
                      <th>Total Tagihan</th>
                      <th></th>
                    </tr>
                  </thead>
                </table>
              </div>
          </div>
        </div>
      </div>
@endsection

@section('js')
  <script type="text/javascript">
    $(function() {
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ url('billings/data') }}",
            columns: [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'name', name: 'name' },
                { data: 'total_billing', name: 'total_billing' },
                { data: 'show', name: 'show', orderable: false, searchable: false },
            ]
        })
    })
  </script>
@endsection
