@extends('layouts.backoffice_layout')

@section('title')
  Home
@endsection

@section('sub_title')
  Detail Order
@endsection

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-9">
                    <h4>Detail Order</h4>
                  </div>
                  <div class="col-md-2">
                    @if ($billing->status == 'billed')
                      <a href="{{ url('billings/paid/'.$billing->id) }}" class="btn btn-success">Paid</a>
                    @else
                      <a href="#" class="btn btn-success" disabled>Tagihan sudah dibayar</a>
                    @endif
                  </div>
                </div>
              </div>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-3">
                    <b>Nomor Billing: </b>
                  </div>
                  <div class="col-md-8">
                    <b>{{ $billing->id }}</b>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    Nama Debitur:
                  </div>
                  <div class="col-md-8">
                    {{ $billing->user->name }}
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    Tanggal Tagihan:
                  </div>
                  <div class="col-md-8">
                    {{ date('d M Y', strtotime($billing->billing_date)) }}
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <h4 class="box-title">Detail Pesanan</h4>
              </div>
              <div class="box-body">
                @php($salim = 1)
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>No.</th>
                      <th>Nomor Order</th>
                      <th>Total Tagihan</th>
                      <th>Deskripsi Pesanan</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($orders as $order)
                      <tr>
                        <td>{{ $salim++ }}</td>
                        <td>{{ $order->id }}</td>
                        <td>{{ number_format($order->details->sum('price')) }}</td>
                        <td>
                          <ul>
                            @foreach ($order->details as $detail)
                              <li>
                                {{ $detail->cake->name }} <br>
                                Rp. {{ number_format($detail->price) }} x {{ $detail->quantity }}pc(s) <br>
                                Total = {{ number_format($detail->price * $detail->quantity) }}
                              </li>
                            @endforeach
                          </ul>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
          </div>
        </div>
      </div>
@endsection
