@php($salim = 0)
@foreach ($ingredients as $ingredient)
  <div class="row">
    <div class="col-md-3">
      <input type="hidden" class="ingredients_id" name="ingredients_id[{{ $salim }}]" value="{{ $ingredient->id }}">
      <label>Nama Bahan: {{ $ingredient->name }} </label>
    </div>
    <div class="col-md-2">
      <div class="row">
        <div class="col-md-4">
          <label>Jumlah: </label>
        </div>
        <div class="col-md-8">
          <div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
            <input type="number" class="form-control ingredients_quantity" name="quantity[{{ $salim }}]" min="1" value="{{ old('quantity') }}" placeholder="Jumlah" required>
            <small class="text-danger">{{ $errors->first('quantity') }}</small>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="row">
        <div class="col-md-4">
          <label>Harga: </label>
        </div>
        <div class="col-md-8">
          <div class="form-group{{ $errors->has('cost') ? ' has-error' : '' }}">
            <input type="text" class="form-control ingredients_cost" name="cost[{{ $salim }}]" value="{{ $ingredient->cost }}" placeholder="Cost" readonly required>
            <small class="text-danger">{{ $errors->first('cost') }}</small>
          </div>
        </div>
      </div>
    </div>
  </div>

  @php($salim++)
@endforeach
