@extends('layouts.backoffice_layout')

@section('title')
  Home
@endsection

@section('sub_title')
  Detail User
@endsection

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Detail Kue</h4>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-8">
                    <div class="row">
                      <div class="col-md-3">
                        <label>Nama Kue </label>
                      </div>
                      <div class="col-md-7">
                          : {{ $cake->name }}
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <label>Total Harga Beli: </label>
                      </div>
                      <div class="col-md-7">
                          : {{ number_format($cake->cost) }}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Detail Bahan Kue</h4>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>
              <div class="box-body">
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>No.</th>
                      <th>Nama</th>
                      <th>Harga</th>
                      <th>Jumlah</th>
                      <th>Total</th>
                    </tr>
                  </thead>
                  <tbody>
                    @php($salim = 1)
                    @foreach ($cake->ingredients as $ingredient)
                      <tr>
                        <td>{{ $salim++ }}</td>
                        <td>{{ $ingredient->ingredient->name }}</td>
                        <td>{{ number_format($ingredient->cost) }}</td>
                        <td>{{ $ingredient->quantity }}</td>
                        <td>{{ number_format($ingredient->quantity * $ingredient->cost) }}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
          </div>
        </div>
      </div>
@endsection
