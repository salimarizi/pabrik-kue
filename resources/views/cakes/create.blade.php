@extends('layouts.backoffice_layout')

@section('css')
  <link rel="stylesheet" href="{!! asset('admin_layouts/plugins/select2/select2.min.css') !!}">
@endsection

@section('title')
  Kue
@endsection

@section('sub_title')
  Tambah Kue
@endsection

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Form Kue</h4>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>
              <div class="box-body">
              <form action="{{ url('cakes') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                  <div class="col-md-2">
                    <label>Nama Kue: </label>
                  </div>
                  <div class="col-md-7">
                      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                          <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Nama Kue" required>
                          <small class="text-danger">{{ $errors->first('name') }}</small>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Bahan: </label>
                  </div>
                  <div class="col-md-7">
                    <div class="form-group{{ $errors->has('ingredient_id') ? ' has-error' : '' }}">
                        <select class="form-control select2" id="ingredients" multiple name="ingredient_id">
                          @foreach ($ingredients as $ingredient)
                            <option value="{{ $ingredient->id }}">{{ $ingredient->name }}</option>
                          @endforeach
                        </select>
                        <small class="text-danger">{{ $errors->first('ingredient_id') }}</small>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <button type="button" class="btn btn-primary" id="submitBahan">
                      Submit
                    </button>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Bahan-bahan</h4>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>
              <div class="box-body" id="ingredients_data">

              </div>
            </div>
          </div>
        </div>

      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4></h4>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-2">
                    <label>Harga Modal: </label>
                  </div>
                  <div class="col-md-4">
                      <div class="form-group{{ $errors->has('cost') ? ' has-error' : '' }}">
                          <input type="text" class="form-control" name="cost" id="total_cost" value="{{ old('cost') }}" placeholder="Harga Kue" readonly required>
                          <small class="text-danger">{{ $errors->first('cost') }}</small>
                      </div>
                  </div>
                  <div class="col-md-3">
                    <button type="button" class="btn btn-success" id="calculateHPP">Hitung Harga Modal</button>
                  </div>
                </div>
              </div>
              <div class="box-footer">
                <input type="hidden" name="ingredients_summary" value="">
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                </form>
              </div>
            </div>
          </div>
        </div>
@endsection


@section('js')
  <script src="{!! asset('admin_layouts/plugins/select2/select2.full.min.js') !!}"></script>
  <script type="text/javascript">
    $('.select2').select2()

    $("#ingredients").select2({
      placeholder : 'Bahan-bahan'
    })

    $('#submitBahan').on('click', () => {
      $.ajax({
        type: 'GET',
        url: '{{ url("cakes/getIngredientsData") }}',
        data: {ingredients: $('#ingredients').val()},
        success: (data) => {
          $('#ingredients_data').html(data)
        },
        error: (error) => {
          console.log(error);
        }
      })
    })

    $('#calculateHPP').on('click', () => {
      total_cost = 0
      ingredients = []
      quantity = []
      cost = []

      $('.ingredients_quantity').each(function(index) {
          ingredients[index] = $('.ingredients_id:eq('+index+')').val()
          quantity[index] = $(this).val()
          cost[index] = $('.ingredients_cost:eq('+index+')').val()

          total_cost += ($(this).val() * parseInt($('.ingredients_cost:eq('+index+')').val()))
      })

      $('input[name=ingredients_summary]').val(ingredients +';'+ quantity +';'+ cost)

      $('#total_cost').val(total_cost)
    })
  </script>
@endsection
