@extends('layouts.backoffice_layout')

@section('css')
  <link rel="stylesheet" href="{!! asset('admin_layouts/plugins/select2/select2.min.css') !!}">
@endsection

@section('title')
  Bahan
@endsection

@section('sub_title')
  Tambah Bahan
@endsection

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Form Bahan</h4>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>
              <div class="box-body">
              <form action="{{ url('ingredients') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                  <div class="col-md-2">
                    <label>Nama Bahan: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                          <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Nama Bahan" required>
                          <small class="text-danger">{{ $errors->first('name') }}</small>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Kategori: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('ingridient_category_id') ? ' has-error' : '' }}">
                          <select class="form-control" name="ingridient_category_id">
                            @foreach ($categories as $category)
                              <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                          </select>
                          <small class="text-danger">{{ $errors->first('ingridient_category_id') }}</small>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Harga: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('cost') ? ' has-error' : '' }}">
                          <input type="text" class="form-control" name="cost" value="{{ old('cost') }}" placeholder="Harga Beli Bahan" required>
                          <small class="text-danger">{{ $errors->first('cost') }}</small>
                      </div>
                  </div>
                </div>

                {{-- <div class="row">
                  <div class="col-md-2">
                    <label>Gambar: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                          <input type="file" class="form-control" name="image">
                          <small class="text-danger">{{ $errors->first('image') }}</small>
                      </div>
                  </div>
                </div> --}}
              </div>

              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                </form>
              </div>
          </div>
        </div>
      </div>
@endsection


@section('js')
  <script src="{!! asset('admin_layouts/plugins/select2/select2.full.min.js') !!}"></script>
  <script type="text/javascript">
    $('.select2').select2()

    $("#divisions").select2({
      placeholder : 'Pilih divisi'
    })
  </script>
@endsection
