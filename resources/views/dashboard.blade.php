@extends('layouts.backoffice_layout')

@section('css')
  <link rel="stylesheet" href="{!! asset('amcharts/style.css') !!}" type="text/css">
  <script src="{!! asset('amcharts/amcharts.js') !!}" type="text/javascript"></script>
  <script src="{!! asset('amcharts/serial.js') !!}" type="text/javascript"></script>
@endsection

@section('title')
  Dashboard
@endsection

@section('sub_title')
  Dashboard
@endsection

@section('content')
      <h1 align="center">Hi, {{ Auth::user()->name }}</h1>

      @if (Auth::user()->role == 'admin')
        <div class="row">
          <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
              <div class="inner" style="text-align: center">
                <h3>{{ count($users) }}</h3>
                <p>Total Users</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="{{ url('users') }}" class="small-box-footer">Lihat Detail <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
              <div class="inner" style="text-align: center">
                <h3>{{ count($users->where('role', 'staff')) }}</h3>
                <p>Total Pekerja</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="{{ url('users') }}" class="small-box-footer">Lihat Detail <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
              <div class="inner" style="text-align: center">
                <h3>{{ count($users->where('role', 'mitra')) }}</h3>
                <p>Total Mitra</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="{{ url('users') }}" class="small-box-footer">Lihat Detail <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
        </div>
      @endif

      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Dashboard</h4>
                  </div>
                </div>
              </div>
              <div class="box-body">
                <div class="row">
                  @if (Auth::user()->role == 'admin' || Auth::user()->role == 'staff')
                    <div class="col-md-10 col-md-offset-1">
                      <div id="columnChart" style="width: 100%; height: 300px;"></div>
                    </div>
                  @endif
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <h4>Daftar Transaksi Terakhir</h4>
                    <table class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th>No.</th>
                          <th>Nomor Pesanan</th>
                          <th>Nama</th>
                          <th>Total Harga</th>
                          <th>Deskripsi</th>
                        </tr>
                      </thead>
                      <tbody>
                        @php($salim = 1)
                        @foreach ($orders as $order)
                          <tr>
                            <td>{{ $salim++ }}</td>
                            <td>{{ $order->id }}</td>
                            <td>{{ $order->buyer->name }}</td>
                            <td>{{ number_format($order->details->sum('price')) }}</td>
                            <td>
                              <ul>
                                @foreach ($order->details as $detail)
                                  <li>
                                    {{ $detail->cake->name }} <br>
                                    Rp. {{ number_format($detail->price) }} x {{ $detail->quantity }}pc(s) <br>
                                    Total = {{ number_format($detail->price * $detail->quantity) }}
                                  </li>
                                @endforeach
                              </ul>
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
@endsection

@section('js')
  <script>
      var chart;

      var chartData = [
        @foreach ($sales_by_day as $date)
          {
              "date": "{{ $date['date'] }}",
              "total_sales": {{ $date['total_sales'] }},
              "total_cost": {{ $date['total_cost'] }},
              "total_profit": {{ $date['total_profit'] }}
          },
        @endforeach
      ];

      AmCharts.ready(function () {
          // SERIAL CHART
          chart = new AmCharts.AmSerialChart();
          chart.dataProvider = chartData;
          chart.categoryField = "date";
          chart.color = "#FFFFFF";
          chart.fontSize = 14;
          chart.startDuration = 1;
          chart.plotAreaFillAlphas = 0.2;
          // the following two lines makes chart 3D
          chart.angle = 30;
          chart.depth3D = 60;
          chart.addTitle("Sales by Day");

          // AXES
          // category
          var categoryAxis = chart.categoryAxis;
          categoryAxis.gridAlpha = 0.2;
          categoryAxis.gridPosition = "start";
          categoryAxis.gridColor = "#FFFFFF";
          categoryAxis.axisColor = "#FFFFFF";
          categoryAxis.axisAlpha = 0.5;
          categoryAxis.dashLength = 5;

          // value
          var valueAxis = new AmCharts.ValueAxis();
          valueAxis.stackType = "3d"; // This line makes chart 3D stacked (columns are placed one behind another)
          valueAxis.gridAlpha = 0.2;
          valueAxis.gridColor = "#FFFFFF";
          valueAxis.axisColor = "#FFFFFF";
          valueAxis.axisAlpha = 0.5;
          valueAxis.dashLength = 5;
          valueAxis.title = "Sales rate";
          valueAxis.titleColor = "#FFFFFF";
          valueAxis.unit = "%";
          chart.addValueAxis(valueAxis);

          // GRAPHS
          // first graph
          var graph1 = new AmCharts.AmGraph();
          graph1.title = "Total Sales";
          graph1.valueField = "total_sales";
          graph1.type = "column";
          graph1.lineAlpha = 0;
          graph1.lineColor = "#00A3CB";
          graph1.fillAlphas = 1;
          graph1.balloonText = "Sales in [[category]] (Total Sales): <b>[[value]]</b>";
          chart.addGraph(graph1);

          var graph2 = new AmCharts.AmGraph();
          graph2.title = "Total Cost";
          graph2.valueField = "total_cost";
          graph2.type = "column";
          graph2.lineAlpha = 0;
          graph2.lineColor = "#DD4B39";
          graph2.fillAlphas = 1;
          graph2.balloonText = "Sales in [[category]] (Total Cost): <b>[[value]]</b>";
          chart.addGraph(graph2);

          var graph3 = new AmCharts.AmGraph();
          graph3.title = "Total Profit";
          graph3.valueField = "total_profit";
          graph3.type = "column";
          graph3.lineAlpha = 0;
          graph3.lineColor = "#00A65A";
          graph3.fillAlphas = 1;
          graph3.balloonText = "Sales in [[category]] (Total Profit): <b>[[value]]</b>";
          chart.addGraph(graph3);

          chart.write("columnChart");
      });
  </script>
@endsection
