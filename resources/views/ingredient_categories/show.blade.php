@extends('layouts.backoffice_layout')

@section('title')
  Home
@endsection

@section('sub_title')
  Detail User
@endsection

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Detail User</h4>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4">
                    @if ($user->image)
                      <img src="{{ url('storage/users/'.$user->image) }}" class="img img-responsive">
                    @else
                      <img src="{{ url('default_images/user.png') }}" class="img img-responsive">
                    @endif
                  </div>
                  <div class="col-md-8">
                    <div class="row">
                      <div class="col-md-3">
                        <label>Nama Lengkap </label>
                      </div>
                      <div class="col-md-7">
                          : {{ $user->name }}
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <label>Level </label>
                      </div>
                      <div class="col-md-7">
                          : {{ $user->role_name }}
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <label>Email </label>
                      </div>
                      <div class="col-md-7">
                          : {{ $user->email }}
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <label>Alamat </label>
                      </div>
                      <div class="col-md-7">
                        : {{ $user->address }}
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <label>No. Handphone </label>
                      </div>
                      <div class="col-md-7">
                        : {{ $user->phone_number }}
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <label>Divisi </label>
                      </div>
                      <div class="col-md-7">
                        <ul>
                          @foreach ($divisions as $division)
                            <li>{{ $division->name }}</li>
                          @endforeach
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
@endsection
