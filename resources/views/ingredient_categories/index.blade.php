@extends('layouts.backoffice_layout')

@section('title')
  Kategori Bahan
@endsection

@section('sub_title')
  List Kategori Bahan
@endsection

@section('modal')
  <div class="modal" id="modal-delete">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus Kategori Bahan</h4>
        </div>
        <div class="modal-body">
          <form action="{{ url('ingredient_categories') }}" method="post" id="form-delete">
            @csrf
            {{ method_field('DELETE') }}
            Apakah Anda yakin ingin menghapus user :
            <b><span id="ingredient_category_name"></span></b>?
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
              Close
            </button>
            <button type="submit" class="btn btn-danger">
              Hapus
            </button>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Daftar Kategori Bahan</h4>
                  </div>
                  <div class="col-md-2">
                    <a href="{{ url('ingredient_categories/create') }}" class="btn btn-primary">
                      <i class="fa fa-plus"></i> Tambah Kategori Bahan
                    </a>
                  </div>
                </div>
              </div>
              <div class="box-body">
                <table id="table" class="table table-bordered table-striped table-hover">
                  <thead>
                    <tr>
                      <th>No.</th>
                      <th>Nama Kategori Bahan</th>
                      <th></th>
                      <th></th>
                      <th></th>
                    </tr>
                  </thead>
                </table>
              </div>
          </div>
        </div>
      </div>
@endsection

@section('js')
  <script type="text/javascript">
    $(function() {
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ url('ingredient_categories/data') }}",
            columns: [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'name', name: 'name' },
                { data: 'edit', name: 'edit', orderable: false, searchable: false },
                { data: 'delete', name: 'delete', orderable: false, searchable: false },
                { data: 'show', name: 'show', orderable: false, searchable: false },
            ]
        })
    })

    deleteModal = (id, name) => {
      $('#modal-delete #ingredient_category_name').text(name)
      $('#modal-delete #form-delete').attr('action', "{{ url('ingredient_categories') }}/" + id)
    }
  </script>
@endsection
