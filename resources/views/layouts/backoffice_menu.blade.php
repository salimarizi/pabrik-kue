<ul class="sidebar-menu">
  <li class="header">MENU</li>
  <!-- Optionally, you can add icons to the links -->
  <li>
    <a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i>
      <span>Dashboard</span>
    </a>
  </li>

  <li>
    <a href="{{ url('orders') }}"><i class="fa fa-money"></i>
      <span>Pesanan</span>
    </a>
  </li>

  <li>
    <a href="{{ url('billings') }}"><i class="fa fa-money"></i>
      <span>Tagihan</span>
    </a>
  </li>

  @if (Auth::user()->role == 'staff')
    <li>
      <a href="{{ url('ingredient_categories') }}"><i class="fa fa-book"></i>
        <span>Kategori Bahan</span>
      </a>
    </li>

    <li>
      <a href="{{ url('ingredients') }}"><i class="fa fa-book"></i>
        <span>Bahan</span>
      </a>
    </li>

    <li>
      <a href="{{ url('cakes') }}"><i class="fa fa-book"></i>
        <span>Kue</span>
      </a>
    </li>
  @endif

  @if (Auth::user()->role == 'admin')
    <li>
      <a href="{{ url('users') }}"><i class="fa fa-users"></i>
        <span>User</span>
      </a>
    </li>
  @endif
</ul>
