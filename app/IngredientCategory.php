<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IngredientCategory extends Model
{
    protected $table = 'ingredient_categories';

    protected $fillable = [
      'name'
    ];
}
