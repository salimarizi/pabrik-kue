<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cake extends Model
{
    protected $table = 'cakes';

    protected $fillable = [
      'name',
      'cost'
    ];

    public function ingredients()
    {
        return $this->hasMany(CakeIngredientRelation::class);
    }
}
