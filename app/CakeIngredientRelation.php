<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CakeIngredientRelation extends Model
{
    protected $table = 'cake_ingredient_relations';

    protected $fillable = [
      'cake_id',
      'ingredient_id',
      'quantity',
      'cost'
    ];

    public function cake()
    {
        return $this->belongsTo(Cake::class);
    }

    public function ingredient()
    {
        return $this->belongsTo(Ingredient::class);
    }
}
