<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Billing extends Model
{
    protected $table = 'billings';

    protected $fillable = [
      'user_id',
      'total_billing',
      'billing_date',
      'status'
    ];

    public function details()
    {
        return $this->hasMany(BillingDetail::class, 'billing_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
