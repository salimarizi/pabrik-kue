<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillingDetail extends Model
{
    protected $table = 'billing_details';

    protected $fillable = [
      'billing_id',
      'order_id'
    ];

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }
}
