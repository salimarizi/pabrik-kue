<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $table = 'order_details';

    protected $fillable = [
      'order_id',
      'cake_id',
      'quantity',
      'cost',
      'price',
      'status',
      'expire_date'
    ];

    public function cake()
    {
        return $this->belongsTo(Cake::class, 'cake_id');
    }
}
