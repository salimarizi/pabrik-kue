<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use Auth;

use App\Order;
use App\OrderDetail;
use App\Cake;
use App\User;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function loadData()
    {
        if (Auth::user()->role == 'mitra') {
          $orders = Order::where('user_id', Auth::user()->id)
                         ->orderBy('id', 'desc')
                         ->get();
        }else {
          $orders = Order::orderBy('id', 'desc')->get();
        }
        return DataTables::of($orders)
              ->addIndexColumn()
              ->addColumn('name', function ($order) {
                return $order->buyer->name;
              })
              ->addColumn('total_cost', function ($order) {
                $total_cost = 0;
                foreach ($order->details as $detail) {
                  $total_cost += $detail->cost;
                }
                return number_format($total_cost);
              })
              ->addColumn('total_price', function ($order) {
                $total_price = 0;
                foreach ($order->details as $detail) {
                  $total_price += $detail->price;
                }
                return number_format($total_price);
              })
              ->addColumn('edit', function ($order) {
                  if (Auth::user()->role == 'staff') {
                    $url = url('orders/'.$order->id.'/edit');
                    return '<a href="'.$url.'" class="btn btn-success">
                      <i class="fa fa-edit"></i>
                    </a>';
                  }
                  return '';
              })
              ->addColumn('delete', function ($order) {
                  if (Auth::user()->role == 'admin') {
                    return '<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete" onclick="deleteModal('.$order->id.',\''.@$order->buyer->name.'\')">
                      <i class="fa fa-trash"></i>
                    </button>';
                  }
                  return '';
              })
              ->addColumn('show', function ($order) {
                  $url = url('orders/'.$order->id);
                  return '<a href="'.$url.'" class="btn btn-primary">
                    <i class="fa fa-chevron-right"></i>
                  </a>';
              })
              ->rawColumns(['edit', 'delete', 'show'])
              ->make(true);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('orders.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::where('role', 'mitra')->get();
        $cakes = Cake::all();
        return view('orders.create', compact('users', 'cakes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $cakes_summary = explode(';', $data['cakes_summary']);
        $cakes_id = explode(',', $cakes_summary[0]);
        $cakes_quantity = explode(',', $cakes_summary[1]);
        $cakes_price = explode(',', $cakes_summary[2]);

        $data['order_date'] = date('Y-m-d');
        $data['shipping_price'] = 0;
        $data['status'] = 'ordered';

        $order_id = Order::create($data)->id;

        for ($i=0; $i < count($cakes_id); $i++) {
          OrderDetail::create([
            'order_id' => $order_id,
            'cake_id' => $cakes_id[$i],
            'quantity' => $cakes_quantity[$i],
            'cost' => Cake::find($cakes_id[$i])->cost,
            'price' => $cakes_price[$i],
            'status' => 'ordered',
            'expire_date' => date('Y-m-d', strtotime('+7 days'))
          ]);
        }

        return redirect('orders');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        return view('orders.show', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        $users = User::where('role', 'mitra')->get();
        $cakes = Cake::all();
        return view('orders.edit', compact('users', 'cakes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        $data = $request->all();

        $order->update($data);

        OrderDetail::where('order_id', $order->id);

        foreach ($order_details as $order_detail) {
          OrderDetail::create([
            'order_id' => $order->id,
            'cake_id' => $order_detail['cake_id'],
            'quantity' => $order_detail['quantity'],
            'price' => $order_detail['price']
          ]);
        }

        return redirect('orders');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        $order->delete();
        return redirect('orders');
    }

    public function getCakesData(Request $request)
    {
        $cakes = Cake::whereIn('id', $request->cakes)->get();
        return view('orders.cakes_data', compact('cakes'));
    }
}
