<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;

use App\Ingredient;
use App\CakeIngredientRelation;
use App\Cake;

class CakeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function loadData()
    {
        $cakes = Cake::orderBy('id', 'desc');
        return DataTables::of($cakes)
              ->addIndexColumn()
              ->addColumn('edit', function ($cake) {
                  $url = url('cakes/'.$cake->id.'/edit');
                  return '<a href="'.$url.'" class="btn btn-success">
                    <i class="fa fa-edit"></i>
                  </a>';
              })
              ->addColumn('delete', function ($cake) {
                  return '<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete" onclick="deleteModal('.$cake->id.',\''.$cake->name.'\')">
                    <i class="fa fa-trash"></i>
                  </button>';
              })
              ->addColumn('show', function ($cake) {
                  $url = url('cakes/'.$cake->id);
                  return '<a href="'.$url.'" class="btn btn-primary">
                    <i class="fa fa-chevron-right"></i>
                  </a>';
              })
              ->rawColumns(['edit', 'delete', 'show'])
              ->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cakes.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ingredients = Ingredient::all();
        return view('cakes.create', compact('ingredients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $ingredients_summary = explode(';', $data['ingredients_summary']);
        $ingredient_id = explode(',', $ingredients_summary[0]);
        $ingredient_quantity = explode(',', $ingredients_summary[1]);
        $ingredient_cost = explode(',', $ingredients_summary[2]);

        $cake_id = Cake::create($data)->id;

        for ($i=0; $i < count($ingredient_id); $i++) {
          CakeIngredientRelation::create([
            'cake_id' => $cake_id,
            'ingredient_id' => $ingredient_id[$i],
            'quantity' => $ingredient_quantity[$i],
            'cost' => $ingredient_cost[$i]
          ]);
        }

        return redirect('cakes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Cake $cake)
    {
        return view('cakes.show', compact('cake'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Cake $cake)
    {
        $ingredients = Ingredient::all();
        return view('cakes.edit', compact('cake', 'ingredients'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cake $cake)
    {
        $data = $request->all();
        $cake->update($data);
        return redirect('cakes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cake $cake)
    {
        $cake->delete();
        return redirect('cakes');
    }

    public function getIngredientsData(Request $request)
    {
        $ingredients = Ingredient::whereIn('id', $request->ingredients)->get();
        return view('cakes.ingredients_data', compact('ingredients'));
    }
}
