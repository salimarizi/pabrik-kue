<?php

namespace App\Http\Controllers;

use App\IngredientCategory;

use Illuminate\Http\Request;
use DataTables;

class IngredientCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function loadData()
    {
        $ingredient_categories = IngredientCategory::orderBy('id', 'desc');
        return DataTables::of($ingredient_categories)
              ->addIndexColumn()
              ->addColumn('edit', function ($ingredient_category) {
                  $url = url('ingredient_categories/'.$ingredient_category->id.'/edit');
                  return '<a href="'.$url.'" class="btn btn-success">
                    <i class="fa fa-edit"></i>
                  </a>';
              })
              ->addColumn('delete', function ($ingredient_category) {
                  return '<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete" onclick="deleteModal('.$ingredient_category->id.',\''.$ingredient_category->name.'\')">
                    <i class="fa fa-trash"></i>
                  </button>';
              })
              ->addColumn('show', function ($ingredient_category) {
                  $url = url('ingredient_categories/'.$ingredient_category->id);
                  return '<a href="'.$url.'" class="btn btn-primary">
                    <i class="fa fa-chevron-right"></i>
                  </a>';
              })
              ->rawColumns(['edit', 'delete', 'show'])
              ->make(true);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('ingredient_categories.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ingredient_categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $ingredient_category_id = IngredientCategory::create($data)->id;
        return redirect('ingredient_categories');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(IngredientCategory $ingredient_category)
    {
        return view('ingredient_categories.show', compact('ingredient_category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(IngredientCategory $ingredient_category)
    {
        return view('ingredient_categories.edit', compact('ingredient_category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IngredientCategory $ingredient_category)
    {
        $data = $request->all();
        $ingredient_category->update($data);
        return redirect('ingredient_categories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(IngredientCategory $ingredient_category)
    {
        $ingredient_category->delete();
        return redirect('ingredient_categories');
    }
}
