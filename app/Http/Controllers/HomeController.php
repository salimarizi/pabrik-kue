<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\Billing;
use App\Order;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['users'] = User::all();
        $data['sales_by_day'] = $this->getSalesByDay();

        if (Auth::user()->role == 'mitra') {
          $data['orders'] = Order::where('user_id', Auth::user()->id)
                                 ->orderBy('id', 'desc')
                                 ->get()
                                 ->take(5);
        }else {
          $data['orders'] = Order::orderBy('id', 'desc')
                                 ->get()
                                 ->take(5);
        }

        return view('dashboard', $data);
    }

    public function getSalesByDay()
    {
        $datas = [];
        $start_date = date('Y-m-d', strtotime('-14 days'));
        $end_date = date('Y-m-d', strtotime('+3 days'));

        $begin = new \DateTime($start_date);
        $end = new \DateTime($end_date);
        $interval = \DateInterval::createFromDateString('1 day');
        $period = new \DatePeriod($begin, $interval, $end);

        foreach ($period as $dt) {
          $total_cost = 0;
          $billings = Billing::whereDate('billing_date', $dt->format('Y-m-d'))->get();

          foreach ($billings as $billing) {
            foreach ($billing->details as $detail) {
              $total_cost += $detail->order->details->sum('cost');
            }
          }
          array_push($datas, [
            'date' => $dt->format('d M Y'),
            'total_sales' => $billings->sum('total_billing'),
            'total_cost' => $total_cost,
            'total_profit' => $billings->sum('total_billing') - $total_cost
          ]);
        }

        return $datas;
    }
}
