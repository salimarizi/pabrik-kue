<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use Auth;

use App\Billing;
use App\BillingDetail;
use App\User;
use App\Order;

class BillingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function loadData()
    {
        if (Auth::user()->role == 'mitra') {
          $billings = Billing::where('user_id', Auth::user()->id)
                             ->orderBy('id', 'desc')
                             ->get();
        }else {
          $billings = Billing::orderBy('id', 'desc')->get();
        }
        
        return DataTables::of($billings)
              ->addIndexColumn()
              ->addColumn('name', function ($billing) {
                return $billing->user->name;
              })
              ->addColumn('total_billing', function ($billing) {
                return number_format($billing->total_billing);
              })
              ->addColumn('show', function ($billing) {
                  $url = url('billings/'.$billing->id);
                  return '<a href="'.$url.'" class="btn btn-primary">
                    <i class="fa fa-chevron-right"></i>
                  </a>';
              })
              ->rawColumns(['show'])
              ->make(true);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('billings.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::where('role', 'mitra')->get();
        $orders = Order::all();
        return view('billings.create', compact('users', 'orders'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $orders = Order::where('user_id', $request->user_id)
                       ->where('status', 'ordered')
                       ->get();
        $total_billing = 0;

        foreach ($orders as $order) {
          $total_billing += $order->details->sum('price');
        }

        $billing = Billing::create([
          'user_id' => $request->user_id,
          'total_billing' => $total_billing,
          'billing_date' => date('Y-m-d')
        ]);

        foreach ($orders as $order) {
          BillingDetail::create([
            'billing_id' => $billing->id,
            'order_id' => $order->id
          ]);

          $order->update(['status' => 'billed']);
        }

        return redirect('billings');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Billing $billing)
    {
        $orders = Order::whereIn('id', $billing->details()->pluck('order_id')->toArray())
                       ->get();
        return view('billings.show', compact('billing', 'orders'));
    }

    public function getOrderData(Request $request)
    {
        $orders = Order::where('user_id', $request->user_id)
                       ->where('status', 'ordered')
                       ->get();

        return view('billings.orders_data', compact('orders'));
    }

    public function paid(Billing $billing)
    {
        $billing->update(['status' => 'paid']);
        return redirect('billings/'.$billing->id);
    }
}
