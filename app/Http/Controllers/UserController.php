<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use DataTables;
use Session;

use App\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function loadData()
    {
        $users = User::where('role', '!=', 'admin')->orderBy('id', 'desc');
        return DataTables::of($users)
              ->addIndexColumn()
              ->editColumn('role', function ($user) {
                  if ($user->role == 'admin') {
                    return 'Admin';
                  }else if ($user->role == 'staff') {
                    return 'Pekerja';
                  }else {
                    return 'Mitra';
                  }
              })
              ->addColumn('edit', function ($user) {
                  $url = url('users/'.$user->id.'/edit');
                  return '<a href="'.$url.'" class="btn btn-success">
                    <i class="fa fa-edit"></i>
                  </a>';
              })
              ->addColumn('delete', function ($user) {
                  return '<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete" onclick="deleteModal('.$user->id.',\''.$user->name.'\')">
                    <i class="fa fa-trash"></i>
                  </button>';
              })
              ->addColumn('show', function ($user) {
                  $url = url('users/'.$user->id);
                  return '<a href="'.$url.'" class="btn btn-primary">
                    <i class="fa fa-chevron-right"></i>
                  </a>';
              })
              ->rawColumns(['edit', 'delete', 'show'])
              ->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $data = $request->except('password_confirmation');
        $data['password'] = bcrypt($data['password']);

        if ($request->image) {
          $data['image'] = $this->uploadImage($request);
        }

        $user_id = User::create($data)->id;

        return redirect('users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, User $user)
    {
        $data = $request->except('password_confirmation');
        if (!$data['password']) {
          unset($data['password']);
        }else {
          $data['password'] = bcrypt($data['password']);
        }

        if ($request->image) {
          $data['image'] = $this->uploadImage($request);
        }

        $user->update($data);

        $user_id = $user->id;

        return redirect('users');
    }

    public function uploadImage(Request $request)
    {
        $imageName = time().'.'.request()->image->getClientOriginalExtension();
        request()->image->move(public_path('storage/users/'), $imageName);
        return $imageName;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if (count($user->orders)) {
          Session::flash('error', 'Tidak dapat menghapus data user karena memiliki transaksi');
          return redirect('users');
        }

        Session::flash('success', 'Berhasil menghapus data user');
        $user->delete();
        return redirect('users');
    }
}
