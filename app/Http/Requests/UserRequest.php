<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validation = [
          'name'     => 'required',
          'role' => 'required',
          'address' => 'nullable|string',
          'phone' => 'nullable',
          'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ];

        if ($this->method() == 'PATCH') {
          $validation['email'] = 'required|email';
          $validation['password'] = 'nullable|string|confirmed';
        }else {
          $validation['email'] = 'required|email|unique:users';
          $validation['password'] = 'required|string|confirmed';
        }

        return $validation;
    }
}
