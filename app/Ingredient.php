<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    protected $table = 'ingredients';

    protected $fillable = [
        'ingridient_category_id',
        'name',
        'cost'
    ];

    public function category()
    {
        return $this->belongsTo(IngredientCategory::class, 'ingridient_category_id');
    }
}
